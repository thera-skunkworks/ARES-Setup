#!/bin/bash
# this file is needed to use the joystick selection by name method.
# Do NOT edit it unless you are absolutely right of what you are doing.

[[ "\$4" != *retroarch* ]] && exit 0

source "$rootdir/supplementary/joystick-selection/jsfuncs.sh"

system="\$1"

get_configs
# if not using joystick selection by name method, there's nothing to do.
if [[ "\$BYNAME" != "ON" ]]; then
    exit 0
fi

echo "--- start of joystick-selection log" >&2
echo "joystick selection by name is ON!" >&2
js_to_retroarchcfg "\$system" && echo "joystick indexes for \"\$system\" was configured" >&2
js_to_retroarchcfg all && echo "joystick indexes for \"all\" was configured" >&2
echo "--- end of joystick-selection log" >&2