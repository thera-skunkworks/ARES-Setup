#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="simcoupe"
rp_module_desc="SimCoupe SAM Coupe emulator"
rp_module_help="ROM Extensions: .dsk .mgt .sbt .sad\n\nCopy your SAM Coupe games to $romdir/samcoupe."
rp_module_licence="GPL2 https://raw.githubusercontent.com/simonowen/simcoupe/master/License.txt"
rp_module_section="sa"
rp_module_flags=""

function depends_simcoupe() {
    getDepends cmake libsdl2-dev zlib1g-dev libbz2-dev libspectrum-dev
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
	
}

function sources_simcoupe() {
    local branch="master"
    # latest simcoupe requires cmake 3.8.2 - on Stretch older versions throw a cmake error about CXX17
    # dialect support but actually seem to build ok. Lock systems with older cmake to 20200711 tag,
    # which builds ok on Raspbian Stretch and hopefully Ubuntu 18.04.
    hasPackage cmake 3.8.2 lt && branch="20200711"
    gitPullOrClone "$md_build" https://github.com/simonowen/simcoupe.git "$branch"
}

function build_simcoupe() {
    cmake -DCMAKE_INSTALL_PREFIX="$md_inst" .
    make clean
    make
    md_ret_require="$md_build/simcoupe"
}

function install_simcoupe() {
    make install

}

function configure_simcoupe() {
    mkRomDir "samcoupe"
    moveConfigDir "$home/.simcoupe" "$md_conf_root/$md_id"

    addEmulator 1 "$md_id" "samcoupe" "pushd $md_inst; $md_inst/bin/simcoupe autoboot -disk1 %ROM% -fullscreen; popd"
    addSystem "samcoupe"
	
}
