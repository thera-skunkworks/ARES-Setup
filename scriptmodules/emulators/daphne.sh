#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="daphne"
rp_module_desc="Daphne - Laserdisc Emulator"
rp_module_help="ROM Extension: .daphne\n\nCopy your Daphne roms to $romdir/daphne"
rp_module_licence="GPL2 https://raw.githubusercontent.com/RetroArena/daphne-emu/master/COPYING"
rp_module_section="sa"
rp_module_flags="dispmanx !x86 !odroid-n2"

function depends_daphne() {
    getDepends libsdl1.2-dev libvorbis-dev libglew-dev zlib1g-dev
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
	
}

function sources_daphne() {
    gitPullOrClone "$md_build" https://github.com/RetroPie/daphne-emu.git retropie
}

function build_daphne() {
local params=()
    isPlatform "aarch64" && params=(--build=arm)
    cd src/vldp2																													  
    ./configure "${params[@]}"
    make -f Makefile.rp
    cd ..
    ln -sf Makefile.vars.rp Makefile.vars
    make STATIC_VLDP=1
}

function install_daphne() {
    md_ret_files=(
        'sound'
        'pics'
        'daphne.bin'
        'COPYING'
    )
}

function configure_daphne() {
    mkRomDir "daphne"
    mkRomDir "daphne/roms"

  [[ "$md_mode" == "remove" ]] && return
    mkUserDir "$md_conf_root/daphne"
    setDispmanx "$md_id" 1
    if [[ ! -f "$md_conf_root/daphne/dapinput.ini" ]]; then
        cp -v "$md_data/dapinput.ini" "$md_conf_root/daphne/dapinput.ini"
    fi
    ln -snf "$romdir/daphne/roms" "$md_inst/roms"
    ln -sf "$md_conf_root/$md_id/dapinput.ini" "$md_inst/dapinput.ini"

    cat >"$md_inst/daphne.sh" <<_EOF_
#!/bin/bash
dir="\$1"
name="\${dir##*/}"
name="\${name%.*}"

if [[ -f "\$dir/\$name.commands" ]]; then
    params=\$(<"\$dir/\$name.commands")
fi

"$md_inst/daphne.bin" "\$name" vldp -nohwaccel -framefile "\$dir/\$name.txt" -homedir "$md_inst" -fullscreen -x 1920 -y 1080 \$params
_EOF_
    chmod +x "$md_inst/daphne.sh"

    chown -R $user:$user "$md_inst"
    chown -R $user:$user "$md_conf_root/daphne/dapinput.ini"
    if isPlatform "rpi" || isPlatform "odroid-xu" || isPlatform "jetson-nano"; then
    addEmulator 1 "$md_id" "daphne" "$md_inst/daphne.sh %ROM%"
	else
	addEmulator 1 "$md_id" "daphne" "XINIT:$md_inst/daphne.sh %ROM%"
	addEmulator 0 "$md_id" "daphne-desktop" "$md_inst/daphne.sh %ROM%"
	fi
    addSystem "daphne"
	
}
