#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="stratagus"
rp_module_desc="Stratagus - A strategy game engine to play Warcraft I or II, Starcraft, and some similar open-source games"
rp_module_help="Copy your Stratagus games to $romdir/stratagus"
rp_module_licence="GPL2 https://raw.githubusercontent.com/Wargus/stratagus/master/COPYING"
rp_module_section="sa"
rp_module_flags=""

function depends_stratagus() {
    getDepends cmake libsdl1.2-dev libbz2-dev libogg-dev libvorbis-dev libtheora-dev libpng-dev liblua5.1-0-dev libtolua++5.1-dev libfluidsynth-dev libmikmod-dev
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
	
}

function sources_stratagus() {
    gitPullOrClone "$md_build" https://github.com/Wargus/stratagus.git v2.4.3
}

function build_stratagus() {
    mkdir build
    cd build
    cmake -DENABLE_STRIP=ON ..
    make
    md_ret_require="$md_build/build/stratagus"
}

function install_stratagus() {
    md_ret_files=(
        'build/stratagus'
        'COPYING'
    )
}

function configure_stratagus() {
    mkRomDir "stratagus"
     if isPlatform "odroid-n2" || isPlatform "odroid-xu" || isPlatform "rockpro64"; then
    addEmulator 0 "$md_id" "stratagus" "XINIT:$md_inst/stratagus -F -d %ROM%"
	else addEmulator 0 "$md_id" "stratagus" "$md_inst/stratagus -F -d %ROM%"
	fi
    addSystem "stratagus" "Stratagus Strategy Engine" ".wc1 .wc2 .sc .data"
	 
	
}
