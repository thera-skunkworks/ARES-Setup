#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="atari800"
rp_module_desc="Atari 8-bit/800/5200 emulator"
rp_module_help="ROM Extensions: .a52 .bas .bin .car .xex .atr .xfd .dcm .atr.gz .xfd.gz\n\nCopy your Atari800 games to $romdir/atari800\n\nCopy your Atari 5200 roms to $romdir/atari5200 You need to copy the Atari 800/5200 BIOS files (5200.ROM, ATARIBAS.ROM, ATARIOSB.ROM and ATARIXL.ROM) to the folder $biosdir and then on first launch configure it to scan that folder for roms (F1 -> Emulator Configuration -> System Rom Settings)"
rp_module_licence="GPL2 https://raw.githubusercontent.com/atari800/atari800/master/COPYING"
rp_module_section="sa"
rp_module_flags="!odroid-n2"

function depends_atari800() {
    local depends=(libsdl1.2-dev autoconf automake zlib1g-dev libpng-dev)
    isPlatform "rpi" && depends+=(libraspberrypi-dev)
    getDepends "${depends[@]}"
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
	
}

function sources_atari800() {

    gitPullOrClone "$md_build" "https://github.com/atari800/atari800.git" ATARI800_4_2_0
    if isPlatform "rpi"; then
        applyPatch "$md_data/01_rpi_fixes.diff"
    fi
}

function build_atari800() {
    local params=()
    ./autogen.sh
    isPlatform "videocore" && params+=(--target=rpi)
    ./configure --prefix="$md_inst" ${params[@]}
    make clean
    make
    md_ret_require="$md_build/src/atari800"
}

function install_atari800() {
    cd src
    make install
}

function configure_atari800() {
    local params=()
    isPlatform "kms" && params+=("-fullscreen" "-fs-width %XRES%" "-fs-height %YRES%")

    mkRomDir "atari800"
    mkRomDir "atari5200"

    if [[ "$md_mode" == "install" ]]; then
        mkUserDir "$md_conf_root/atari800"

    # move old config if exists to new location
        if [[ -f "$md_conf_root/atari800.cfg" ]]; then
            mv "$md_conf_root/atari800.cfg" "$md_conf_root/atari800/atari800.cfg"
        fi
        moveConfigFile "$home/.atari800.cfg" "$md_conf_root/atari800/atari800.cfg"

       # copy launch script (used for unpacking archives)
        sed "s#EMULATOR#/bin/$md_id#" "$scriptdir/scriptmodules/$md_type/atari800/atari800.sh" >"$md_inst/$md_id.sh"
        chmod a+x "$md_inst/$md_id.sh"
    fi
	local binary="XINIT:$md_inst/atari800.sh"
	if isPlatform "rpi" || isPlatform "odroid-xu"; then
    addEmulator 1 "atari800" "atari800" "$md_inst/atari800.sh %ROM% ${params[*]}"
	addEmulator 1 "atari800-800" "atari800" "$md_inst/atari800.sh %ROM% ${params[*]} -atari"
	addEmulator 1 "atari800-800xl" "atari800" "$md_inst/atari800.sh %ROM% ${params[*]} -xl"
    addEmulator 1 "atari800-5200" "atari5200" "$md_inst/atari800.sh %ROM% ${params[*]} -5200"
	addEmulator 1 "atari800-130xe" "atari800" "$md_inst/atari800.sh %ROM% ${params[*]} -xe"
	else
	addEmulator 1 "atari800" "atari800" "$binary %ROM% ${params[*]}"
	addEmulator 1 "atari800-800" "atari800" "$binary %ROM% ${params[*]} -atari"
	addEmulator 1 "atari800-800xl" "atari800" "$binary %ROM% ${params[*]} -xl"
    addEmulator 1 "atari800-5200" "atari5200" "$binary %ROM% ${params[*]} -5200"
	addEmulator 1 "atari800-130xe" "atari800" "$binary %ROM% ${params[*]} -xe"
	fi
    addSystem "atari800"
	cp -r "$scriptdir/configs/all/retrofe/medium_artwork" "$romdir/atari800/"
    cp -r "$scriptdir/configs/all/retrofe/system_artwork" "$romdir/atari800/"
    addSystem "atari5200"
	cp -r "$scriptdir/configs/all/retrofe/medium_artwork" "$romdir/atari5200/"
    cp -r "$scriptdir/configs/all/retrofe/system_artwork" "$romdir/atari5200/"
	chown -R $user:$user "$romdir/atari800/"
	chown -R $user:$user "$romdir/atari5200/"
	
	
}
