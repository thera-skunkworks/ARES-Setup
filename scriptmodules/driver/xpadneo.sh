#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="xpadneo"
rp_module_desc="Linux Driver for Xbox One Wireless Gamepad"
rp_module_help="Enhanced Linux driver for Xbox One Wireless Gamepad (which is shipped with the Xbox One S)."
rp_module_licence="GPL3 https://raw.githubusercontent.com/atar-axis/xpadneo/master/LICENSE"
rp_module_section="driver"
rp_module_flags="noinstclean"

function _version_xpadneo() {
    cat "$md_inst/VERSION"
}

function depends_xpadneo() {
    local depends=(dkms)
    isPlatform "rpi" && depends+=(raspberrypi-kernel-headers)
    isPlatform "x11" && depends+=(linux-headers-generic)
    getDepends "${depends[@]}"
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function sources_xpadneo() {
    local tag="v0.9"
    gitPullOrClone "$md_build" https://github.com/atar-axis/xpadneo.git "$tag"
    rsync -a --delete "$md_build/hid-xpadneo/" "$md_inst/"
    cp "$md_build/VERSION" "$md_inst/"
    local version="$(_version_xpadneo)"
    sed -i "s/@DO_NOT_CHANGE@/$version/g" "$md_inst/dkms.conf" "$md_inst/src/hid-xpadneo.c"
}

function build_xpadneo() {
    dkmsManager install hid-xpadneo "$(_version_xpadneo)"
}

function remove_xpadneo() {
    dkmsManager remove hid-xpadneo "$(_version_xpadneo)"
}

function configure_xpadneo() {
    [[ "$md_mode" == "remove" ]] && return

    dkmsManager reload hid-xpadneo "$(_version_xpadneo)"
}
