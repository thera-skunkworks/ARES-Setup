#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="wikidocs"
rp_module_desc="Generate mkdocs documentation from wiki"
rp_module_section=""

function depends_wikidocs() {
    getDepends python3 python3-pip libyaml-dev
    pip3 install --upgrade mkdocs mkdocs-material mdx_truly_sane_lists git+https://github.com/cmitu/mkdocs-altlink-plugin
	
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}


function sources_wikidocs() {
    gitPullOrClone "$md_build" https://github.com/RetroPie/RetroPie-Docs.git
    gitPullOrClone "$md_build/docs" https://github.com/RetroPie/retropie-setup.wiki.git

    cp -v "docs/Home.md" "docs/index.md"
    cp -R "$md_build/"{images,stylesheets} "docs/"
}

function build_wikidocs() {
    mkdocs build
}

function install_wikidocs() {
    rsync -a --delete "$md_build/site/" "$__tmpdir/wikidocs/"
    chown -R $user:$user "$__tmpdir/wikidocs"
}

function upload_wikidocs() {
    adminRsync "$__tmpdir/wikidocs/" "docs/" --delete
}
