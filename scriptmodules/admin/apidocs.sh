#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="apidocs"
rp_module_desc="Generate developer documentation"
rp_module_section=""

function depends_apidocs() {
    getDepends doxygen graphviz
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function sources_apidocs() {
    gitPullOrClone "$md_build" https://github.com/Anvil/bash-doxygen.git
}

function build_apidocs() {
    local config="Doxyfile"
    rm -f "$config"
    doxygen -g "$config" >/dev/null

    iniConfig " = " '' "$config"

    iniSet "PROJECT_NAME" "ARES-Setup"
    iniSet "PROJECT_NUMBER" "$__version"

    iniSet "EXTENSION_MAPPING" "sh=C"
    iniSet "QUIET" "YES"
    iniSet "WARN_IF_DOC_ERROR" "NO"
    iniSet "INPUT" "$scriptdir"
    iniSet "EXCLUDE_PATTERNS" "*/tmp/*"
    iniSet "INPUT_FILTER" "\"sed -n -f $md_build/doxygen-bash.sed -- \""
    iniSet "RECURSIVE" "YES"

    # unable to use iniSet for latest doxygen "multi line" FILE_PATTERNS default
    echo "FILE_PATTERNS = *.sh" >>"$config"

    doxygen "$config"
}

function install_apidocs() {
    rsync -a --delete "$md_build/html/" "$__tmpdir/apidocs/"
    chown -R $user:$user "$__tmpdir/apidocs"
}

function upload_apidocs() {
    adminRsync "$__tmpdir/apidocs/" "api/" --delete
}
